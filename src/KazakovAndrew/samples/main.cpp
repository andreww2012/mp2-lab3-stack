#include "tstack.h"

#include <iostream>

int main() {
  setlocale(LC_ALL, "Russian");
  TStack st(2);
  int temp;

  std::cout << "Тестирование программ поддержки структуры типа стека"
  << std::endl;

  for (int i = 0; i < 35; i++) {
    st.Put(i);
    std::cout << "Положили значение " << i << " Код " << st.GetRetCode()
    << std::endl;
  }

  while (!st.IsEmpty()) {
    temp = st.Get();
    std::cout << "Взяли значение " << temp << " Код " << st.GetRetCode()
    << std::endl;
  }

  return 0;
}
