#include "tsimplestack.h"

#include <gtest.h>

TEST(TSimpleStack, can_create_stack) {
  ASSERT_NO_THROW(TSimpleStack<int>);
}

TEST(TSimpleStack, can_create_stack_of_floats) {
  ASSERT_NO_THROW(TSimpleStack<float>);
}

TEST(TSimpleStack, can_create_empty_stack) {
  ASSERT_NO_THROW(TSimpleStack<int>(5));
}

TEST(TSimpleStack, cant_create_stack_with_negative_size) {
  ASSERT_ANY_THROW(TSimpleStack<int>(-1));
}

TEST(TSimpleStack, can_copy_stack) {
  TSimpleStack<int> st(2);

  ASSERT_NO_THROW(TSimpleStack<int> st_ = st);
}

TEST(TSimpleStack, original_and_copied_stacks_are_equiv) {
  TSimpleStack<int> st(2);

  st.push(1);
  st.push(2);

  TSimpleStack<int> st_ = st;

  EXPECT_EQ(2, st_.pop());
  EXPECT_EQ(1, st_.pop());
}

TEST(TSimpleStack, can_push_element) {
  TSimpleStack<int> st(2);

  ASSERT_NO_THROW(st.push(1));
}

TEST(TSimpleStack, cant_push_element_in_full_stack) {
  TSimpleStack<int> st(2);

  st.push(1);
  st.push(2);

  ASSERT_ANY_THROW(st.push(3));
}

TEST(TSimpleStack, can_get_element) {
  TSimpleStack<int> st(2);

  st.push(1);

  ASSERT_NO_THROW(st.pop());
}

TEST(TSimpleStack, cant_get_element_from_empty_stack) {
  TSimpleStack<int> st(2);

  ASSERT_ANY_THROW(st.pop());
}
