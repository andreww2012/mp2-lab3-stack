#include "arexp_comp.cpp"

#include <gtest.h>

TEST(convert_to_postfix_form, can_convert_to_postfix_form_correctly) {
  EXPECT_EQ("(6*(5 - 3) /3)* 7+4 * 2-5", "6 5 3-* 3/ 7* 4 2*+ 5-");
}

TEST(compute, can_compute_expression_correctly) {
  EXPECT_EQ("(6*(5 - 3) /3)* 7+4 * 2-5", 31);
}

TEST(is_correct, correct_expression_is_correct) {
  EXPECT_EQ("(6*(5 - 3) /3)* 7+4 * 2-5", 0);
}

TEST(is_correct, incorrect_expression_contains_errors) {
  EXPECT_EQ("(a+b1)/2+6.5)*(4.8+sin(5)", 2);
}
