#include "tstack.h"

#include <gtest.h>

TEST(TStack, initial_stack_is_empty) {
  TStack st;
  EXPECT_EQ(st.IsEmpty(), true);
}

TEST(TStack, empty_stack_is_empty_and_full_simultaneously) {
  TStack st(0);
  EXPECT_EQ(st.IsEmpty(), st.IsFull());
}

TEST(TStack, cant_get_anything_from_empty_stack) {
  TStack st(2);
  EXPECT_EQ(st.Get(), -1);
}

TEST(TStack, can_put_and_get) {
  TStack st(4);
  st.Put(1);
  st.Put(2);
  EXPECT_EQ(st.Get(), 2);
}

TEST(TStack, cant_add_sth_to_empty_stack) {
  TStack st(0);
  st.Put(1);
  EXPECT_EQ(st.GetRetCode(), DataFull);
}

TEST(TStack, cant_add_sth_to_full_stack) {
  TStack st(2);
  st.Put(1);
  st.Put(2);
  st.Put(3);
  EXPECT_EQ(st.GetRetCode(), DataFull);
}

TEST(TStack, checking_empty_stack) {
  TStack st(2);
  st.Put(1);
  st.Put(2);
  st.Get(); st.Get();
  EXPECT_EQ(st.IsEmpty(), true);
}

TEST(TStack, checking_full_stack) {
  TStack st(2);
  st.Put(1);
  st.Put(2);
  EXPECT_EQ(st.IsFull(), true);
}

TEST(TStack, ) {
  TStack st;
}
