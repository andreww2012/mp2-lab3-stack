# Отчёт по выполненной работе "Структура данных Стек"
## Введение

**Цель данной работы** — практическое освоение динамической структуры данных **стек**. В качестве области приложений выбрана тема вычисления арифметических выражений, возникающей при трансляции программ на языке программирования высокого уровня в исполняемые программы.

При вычислении произвольных арифметических выражений возникают две основные задачи: проверка корректности введённого выражения и выполнение операций в порядке, определяемом их приоритетами и расстановкой скобок. Существует алгоритм, позволяющий реализовать вычисление произвольного арифметического выражения за один просмотр без хранения промежуточных результатов. Для реализации данного алгоритма выражение должно быть представлено в постфиксной форме. Рассматриваемые в данной лабораторной работе алгоритмы являются начальным введением в область машинных вычислений.

## Виды реализованных стеков
В данной работе представлены два вида стеков: простой, основанный на шаблонах и статическом массиве, и более сложный, основанный на использовании динамической структуры данных.

Далее, используя эти стеки, будет написана программа вычисления арифметического выражения.

## Реализация простого стека (класс TSimpleStack)
Здесь мы имеем вполне очевидные для стека методы: push добавляет элемент в стек, pop извлекает и удаляет последний элемент стека. В процессе пользования данным классом могут возникнуть следующие исключения:
* **1** — задан отрицательный размер стека.
* **2** — стек полон.
* **3** — стек пуст.

```C++
#ifndef __TSIMPLESTACK_H__
#define __TSIMPLESTACK_H__

#define DEFAUT_SIZE 100

template <class data_type = int>
class TSimpleStack {
private:
	int top, size;
	data_type* data;

public:
	TSimpleStack(int = DEFAUT_SIZE);
	TSimpleStack(const TSimpleStack&);
	void push(const data_type& Val);
	data_type pop();
	void print();
};

template <class data_type>
TSimpleStack<data_type>::TSimpleStack(int s): top(-1) {
	
	if (s < 0) {
		throw 1;
	}

	size = s;
	data = new data_type[size];

}

template <class data_type>
TSimpleStack<data_type>::TSimpleStack(const TSimpleStack& st) {

	top = st.top;
	size = st.size;

	data = new data_type[size];

	for (int i = 0; i <= top; i++) {
		data[i] = st.data[i];
	}

}

template <class data_type>
void TSimpleStack<data_type>::push(const data_type& Val) {
	
	if (top == size - 1) {
		throw 2;
	}

	data[++top] = Val;

}

template <class data_type>
data_type TSimpleStack<data_type>::pop() {

	if (top == -1) {
		throw 3;
	}

	return data[top--];

}

template <class data_type>
void TSimpleStack<data_type>::print() {

	for (int i = 0; i <= Top; i++)
		cout << data[i] << ' ';
	cout << endl;

}

#endif
```

### Тесты для проверки корректности реализации, написанные для фреймворка Google Test
```C++
#include "tsimplestack.h"

#include <gtest.h>

TEST(TSimpleStack, can_create_stack) {
	ASSERT_NO_THROW(TSimpleStack<int>);
}

TEST(TSimpleStack, can_create_stack_of_floats) {
	ASSERT_NO_THROW(TSimpleStack<float>);
}

TEST(TSimpleStack, can_create_empty_stack) {
	ASSERT_NO_THROW(TSimpleStack<int>(5));
}

TEST(TSimpleStack, cant_create_stack_with_negative_size) {
	ASSERT_ANY_THROW(TSimpleStack<int>(-1));
}

TEST(TSimpleStack, can_copy_stack) {
	TSimpleStack<int> st(2);

	ASSERT_NO_THROW(TSimpleStack<int> st_ = st);
}

TEST(TSimpleStack, original_and_copied_stacks_are_equiv) {
	TSimpleStack<int> st(2);

	st.push(1);
	st.push(2);

	TSimpleStack<int> st_ = st;

	EXPECT_EQ(2, st_.pop());
	EXPECT_EQ(1, st_.pop());
}

TEST(TSimpleStack, can_push_element) {
	TSimpleStack<int> st(2);

	ASSERT_NO_THROW(st.push(1));
}

TEST(TSimpleStack, cant_push_element_in_full_stack) {
	TSimpleStack<int> st(2);

	st.push(1);
	st.push(2);

	ASSERT_ANY_THROW(st.push(3));
}

TEST(TSimpleStack, can_get_element) {
	TSimpleStack<int> st(2);

	st.push(1);

	ASSERT_NO_THROW(st.pop());
}

TEST(TSimpleStack, cant_get_element_from_empty_stack) {
	TSimpleStack<int> st(2);

	ASSERT_ANY_THROW(st.pop());
}
```

## Реализация более сложного стека (классы TDataCom, TDataRoot, TStack)
Здесь мы уже имеем небольшую иерархию классов.

TDataCom занимается обработкой кодов завершения вызовов методов и, вообще говоря, к стеку не привязан: это наиболее общий класс. 

Абстрактный класс TDataRoot, наследник TDataCom, создан для управления памятью структуры данных стек. TStack наследует TDataRoot и реализует методы этого класса.

### Класс TDataCom
```C++
#ifndef __DATACOM_H__
#define __DATACOM_H__

#define DataOK   0
#define DataErr -1

// TDataCom является общим базовым классом
class TDataCom {
protected:
  int RetCode; // Код завершения
  int SetRetCode(int ret) { return RetCode = ret; }

public:
  TDataCom(): RetCode(DataOK) {}
  virtual ~TDataCom() = 0 {}

  int GetRetCode() {
    int temp = RetCode;
    RetCode = DataOK;
    return temp;
  }
};

#endif
```

### Класс TDataRoot, заголовок и реализация
```C++
#ifndef __DATAROOT_H__
#define __DATAROOT_H__

#include "tdatacom.h"

#define DefMemSize   25  // размер памяти по умолчанию

#define DataEmpty  -101  // СД пуста
#define DataFull   -102  // СД переполнена
#define DataNoMem  -103  // нет памяти

typedef int    TElem;    // тип элемента СД
typedef TElem* PTElem;
typedef int    TData;    // тип значений в СД

enum TMemType { MEM_HOLDER, MEM_RENTER };

class TDataRoot: public TDataCom {
protected:
  PTElem pMem;      // память для СД
  int MemSize;      // размер памяти для СД
  int DataCount;    // количество элементов в СД
  TMemType MemType; // режим управления памятью
  void SetMem(void* p, int Size);	// задание памяти

public:
  virtual ~TDataRoot();
  TDataRoot(int Size = DefMemSize);
  virtual bool IsEmpty(void) const;           // контроль пустоты СД
  virtual bool IsFull (void) const;           // контроль переполнения СД
  virtual void  Put   (const TData &Val) = 0; // добавить значение
  virtual TData Get   (void)             = 0; // извлечь значение

  // служебные методы
  //virtual int  IsValid() = 0;                 // тестирование структуры
  virtual void Print()   = 0;                 // печать значений

  // дружественные классы
  /*friend class TMultiStack;
  friend class TSuperMultiStack;
  friend class TComplexMultiStack;*/
};

#endif

```

```C++
#include "tdataroot.h"

TDataRoot::TDataRoot(int Size): TDataCom() {

	DataCount = 0;
	MemSize = Size;

	if (Size == 0) {
		MemType = MEM_RENTER;
	} else {
		MemType = MEM_HOLDER;
		pMem = new TElem[MemSize];
	}

}

TDataRoot::~TDataRoot() {

	delete[] pMem;

}

void TDataRoot::SetMem(void* p, int Size) {

	if (MemType == MEM_HOLDER) {
		delete[] pMem;
	}

	MemType = MEM_RENTER;
	pMem = (PTElem)p;
	MemSize = Size;

}

bool TDataRoot::IsEmpty(void) const {

	return DataCount == 0;

}

bool TDataRoot::IsFull(void) const {

	return DataCount == MemSize;

}
```

### Класс TStack, заголовок и реализация
```C++
#ifndef __TSTACK_H__
#define __TSTACK_H__

#include "tdataroot.h"

class TStack: public TDataRoot {
private:
	int top;

public:
	TStack(int Size = DefMemSize): TDataRoot(Size), top(-1) {};
	void Put(const TData& Val);
	TData Get();
	void Print();
};

#endif
```

```C++
#include <iostream>
#include "tstack.h"

using namespace std;

void TStack::Put(const TData& Val) {

	if (pMem == nullptr) SetRetCode(DataNoMem);
	else if (IsFull()) SetRetCode(DataFull);
	else {
		pMem[++top] = Val;
		DataCount++;
	}

}

TData TStack::Get() {

	if (pMem == nullptr) SetRetCode(DataNoMem);
	else if (IsEmpty()) SetRetCode(DataEmpty);
	else {
		DataCount--;
		return pMem[top--];
	}
	return -1;

}

void TStack::Print() {

	for (int i = 0; i < DataCount; i++)
		cout << pMem[i] << " ";
	cout << endl;

}
```

### Тесты Google Test
```C++
#include "tstack.h"

#include <gtest.h>

TEST(TStack, initial_stack_is_empty) {
	TStack st;
	EXPECT_EQ(st.IsEmpty(), true);
}

TEST(TStack, empty_stack_is_empty_and_full_simultaneously) {
	TStack st(0);
	EXPECT_EQ(st.IsEmpty(), st.IsFull());
}

TEST(TStack, cant_get_anything_from_empty_stack) {
	TStack st(2);
	EXPECT_EQ(st.Get(), -1);
}

TEST(TStack, can_put_and_get) {
	TStack st(4);
	st.Put(1);
	st.Put(2);
	EXPECT_EQ(st.Get(), 2);
}

TEST(TStack, cant_add_sth_to_empty_stack) {
	TStack st(0);
	st.Put(1);
	EXPECT_EQ(st.GetRetCode(), DataFull);
}

TEST(TStack, cant_add_sth_to_full_stack) {
	TStack st(2);
	st.Put(1);
	st.Put(2);
	st.Put(3);
	EXPECT_EQ(st.GetRetCode(), DataFull);
}

TEST(TStack, checking_empty_stack) {
	TStack st(2);
	st.Put(1);
	st.Put(2);
	st.Get(); st.Get();
	EXPECT_EQ(st.IsEmpty(), true);
}

TEST(TStack, checking_full_stack) {
	TStack st(2);
	st.Put(1);
	st.Put(2);
	EXPECT_EQ(st.IsFull(), true);
}
```

## Программа вычисления арифметического выражения
Метод вычисления арифметического выражения состоит в следующем:
* Проверить корректность выражения, правильность расстановки скобок;
* Перевести в постфиксную (обратную польскую) запись;
* Теперь можно достаточно легко вычислить выражение.

Этот метод основан на применении стека.
```C++
#ifndef __AREXP_COMP_CPP__
#define __AREXP_COMP_CPP__

#include <iostream>
#include <string>
#include "tstack.h"
#include "tsimplestack.h"
#define MAX_STACK_AM 64

using namespace std;

// ��������� ������������ ����������� ������ � ���������. �������� ������� ������������ ������
int is_correct(string str) {

	TStack op_brackets(MAX_STACK_AM);
	int errors = 0, brackets_am = 1;

	for (string::iterator iter = str.begin(); iter != str.end(); ++iter) {
		if (*iter == '(') {
			op_brackets.Put(brackets_am++);
		} else if (*iter == ')') {
			if (op_brackets.IsEmpty()) {
				cout << "0 - " << brackets_am++ << endl;
				errors++;
			} else {
				cout << op_brackets.Get() << " - " << brackets_am++ << endl;
			}
		}
	}

	while (!op_brackets.IsEmpty()) {
		cout << op_brackets.Get() << " - 0" << endl;
		errors++;
	}

	cout << errors << " errors were found." << endl;
		
	return errors;

}

int get_priority(char symb) {
	switch (symb) {
		case '(':
			return 0;
		case ')':
			return 1;
		case '+':
		case '-':
			return 2;
		case '*':
		case '/':
			return 3;
	}

	return -1;
}
 
string convert_to_postfix_form(string str_inf) {

	TStack operations(MAX_STACK_AM);
	string str_postfix;
	int curr_priority, prev_priority;
	char curr_symb, temp_symb;
	bool work_with_operand;

	for (string::iterator iter = str_inf.begin(); iter != str_inf.end(); ++iter) {
		if (*iter == ' ') {
			str_inf.erase(iter);
			--iter;
		}
	}

	for (string::iterator iter = str_inf.begin(); iter != str_inf.end(); ++iter) {

		curr_priority = get_priority(*iter);
		curr_symb = *iter;

		// ���� ������ ������ - ����� ��������
		if (curr_priority == -1) {

			work_with_operand = true;
			str_postfix += curr_symb;

		// ���� ������ ������ - ��������
		} else {

			if (work_with_operand) {
				work_with_operand = false;
				str_postfix += ' ';
			}

			if (curr_symb == ')') {

				while (get_priority(temp_symb = operations.Get()) != 0) {
					str_postfix += temp_symb;
				}
				operations.Get(); // ������������ ����������� ������

			} else if (curr_priority == 0 || curr_priority > prev_priority || operations.IsEmpty()) {
					
				operations.Put(curr_symb);

			} else {

				while (get_priority(temp_symb = operations.Get()) >= curr_priority) {
					str_postfix += temp_symb;
				}
				str_postfix += ' ';
				operations.Put(curr_symb);

			}

		}

		prev_priority = curr_priority;

	} // for

	while (!operations.IsEmpty()) {
		str_postfix += operations.Get();
	}

	return str_postfix;
}

double compute(string str) {

	if (!is_correct(str)) return 0;

	str = convert_to_postfix_form(str);

	TSimpleStack<double> operands(MAX_STACK_AM);
	string temp;
	int curr_priority;
	double first_operand, sec_operand, res;

	for (string::iterator iter = str.begin(); iter != str.end(); ++iter) {

		curr_priority = get_priority(*iter);

		// ���� ������ ������ - ����� ��������
		if (curr_priority == -1 && *iter != ' ') {

			temp += *iter;

		} else {

			if (temp.length()) {
				operands.push(atof(temp.c_str()));
				temp = "";
			}

			if (curr_priority >= 2) {
				sec_operand = operands.pop();
				first_operand = operands.pop();

				switch (*iter) {
				case '+':
					res = first_operand + sec_operand;
					break;
				case '-':
					res = first_operand - sec_operand;
					break;
				case '*':
					res = first_operand * sec_operand;
					break;
				case '/':
					res = first_operand / sec_operand;
					break;
				}

				operands.push(res);
			}
		} // if-else
	} // for

	return res;

}

#endif
```

### Тесты Google Test
```C++
#include "arexp_comp.cpp"

#include <gtest.h>

TEST(convert_to_postfix_form, can_convert_to_postfix_form_correctly)
{
	EXPECT_EQ("(6*(5 - 3) /3)* 7+4 * 2-5", "6 5 3-* 3/ 7* 4 2*+ 5-");
}

TEST(compute, can_compute_expression_correctly)
{
	EXPECT_EQ("(6*(5 - 3) /3)* 7+4 * 2-5", 31);
}

TEST(is_correct, correct_expression_is_correct) {
	EXPECT_EQ("(6*(5 - 3) /3)* 7+4 * 2-5", 0);
}

TEST(is_correct, incorrect_expression_contains_errors) {
	EXPECT_EQ("(a+b1)/2+6.5)*(4.8+sin(5)", 2);
}
```

## Выводы
С помощью незамысловатой структуры данных, стека, была решена кажущаяся на первый взгляд совсем нетривиальныя задача - вычисление арифметического выражения. Понятно, что данный алгоритм перевода в постфиксную форму и вычисления полученного выражения можно применять и в других задачах, где стоит похожая цель, например, в интерпретации программного кода.

Также стоит отметить важность написания тестов для проверки корректности программы. Написав тесты, мы, на самом деле, экономим очень много времени, а не теряем его, поскольку "отлавливаем" большую часть ошибок, даже на ранних стадиях проекта.

