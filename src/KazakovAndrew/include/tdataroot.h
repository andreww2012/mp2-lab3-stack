#ifndef __DATAROOT_H__
#define __DATAROOT_H__

class TDataRoot {
public:
  static const int kDefaultMemSize = 25;
  enum MemMode {
    MEM_HOLDER,
    MEM_RENTER
  };

  explicit TDataRoot(const int size = kDefaultMemSize);
  virtual ~TDataRoot(void);

  virtual bool IsEmpty(void) const;
  virtual bool IsFull(void) const;
  virtual void Put(const int&) = 0;
  virtual int Get(void) = 0;

  virtual void Print(void) = 0;

protected:
  int* memory_;
  int memory_size_;  // размер памяти для структуры данных
  int amount_;  // количество элементов в структуре данных
  MemType mode_;  // режим управления памятью

  void SetMem(void*, const int size);  // задание памяти
};

#endif
