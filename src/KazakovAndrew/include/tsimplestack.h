#ifndef __TSIMPLESTACK_H__
#define __TSIMPLESTACK_H__

#include <iostream>

template <class data_type = int>
class TSimpleStack {
public:
  static const int kDefaultSize = 100;

  explicit TSimpleStack(const int size = kDefaultSize);
  TSimpleStack(const TSimpleStack&);

  void Push(const data_type&);
  data_type Pop(void);
  void Print(void) const;

protected:
  int size_;
  int top_;

  data_type* data_;
};

template <class data_type>
TSimpleStack<data_type>::TSimpleStack(const int size): top_(-1) {
  if (size < 0) {
    throw 1;
  }

  size_ = size;
  data_ = new data_type[size_];
}

template <class data_type>
TSimpleStack<data_type>::TSimpleStack(const TSimpleStack& st) {
  top_ = st.top_;
  size_ = st.size_;

  data_ = new data_type[size_];

  for (int i = 0; i <= top_; i++) {
    data_[i] = st.data_[i];
  }
}

template <class data_type>
void TSimpleStack<data_type>::Push(const data_type& value) {
  if (top_ == size_ - 1) {
    throw 2;
  }

  data_[++top_] = value;
}

template <class data_type>
data_type TSimpleStack<data_type>::Pop() {
  if (top_ == -1) {
    throw 3;
  }

  return data_[top_--];
}

template <class data_type>
void TSimpleStack<data_type>::Print() const {
  for (int i = 0; i <= top_; i++) {
    std::cout << data_[i] << " ";
  }

  std::cout << std::endl;
}

#endif
