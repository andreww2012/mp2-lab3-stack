#ifndef __TSTACK_H__
#define __TSTACK_H__

#include "tdataroot.h"

class TStack : public TDataRoot {
public:
  explicit TStack(const int size = kDefaultMemSize);

  void Put(const int&);
  int Get(void);
  void Print(void) const;

protected:
  int top_;
};

#endif
