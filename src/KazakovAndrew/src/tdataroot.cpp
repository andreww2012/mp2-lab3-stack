#include "tdataroot.h"

TDataRoot::TDataRoot(const int size) {
  amount_ = 0;
  memory_size_ = size;

  if (memory_size_ == 0) {
    mode_ = MemMode::MEM_RENTER;
    memory_ = nullptr;
  } else {
    mode_ = MemMode::MEM_HOLDER;
    memory_ = new int[memory_size_];
  }
}

TDataRoot::~TDataRoot() {
  if (mode_ == MemMode::MEM_HOLDER) {
    delete[] memory_;
  }

  memory_ = nullptr;
}

void TDataRoot::SetMem(void* p, const int size) {
  if (mode_ == MemMode::MEM_HOLDER) {
    delete[] memory_;
  }

  mode_ = MemMode::MEM_RENTER;
  memory_ = (int*)p;
  memory_size_ = size;
}

bool TDataRoot::IsEmpty() const {
  return amount_ == 0;
}

bool TDataRoot::IsFull() const {
  return amount_ == memory_size_;
}
