#include "tstack.h"
#include "tsimplestack.h"

#include <iostream>
#include <string>

#define MAX_STACK_AM 64

// Проверяет правильность расстановки скобок в выражении.
// Печатает таблицу соответствия скобок
int is_correct(std::string str) {
  TStack op_brackets(MAX_STACK_AM);
  int errors = 0, brackets_am = 1;

  for (std::string::iterator iter = str.begin(); iter != str.end(); ++iter) {
    if (*iter == '(') {
      op_brackets.Put(brackets_am++);
    } else if (*iter == ')') {
      if (op_brackets.IsEmpty()) {
        std::cout << "0 - " << brackets_am++ << std::endl;
        errors++;
      } else {
        std::cout << op_brackets.Get() << " - " << brackets_am++ << std::endl;
      }
    }
  }

  while (!op_brackets.IsEmpty()) {
    std::cout << op_brackets.Get() << " - 0" << std::endl;
    errors++;
  }

  std::cout << errors << " errors were found." << std::endl;

  return errors;
}

int get_priority(const char symb) {
  switch (symb) {
    case '(':
      return 0;
    case ')':
      return 1;
    case '+':
    case '-':
      return 2;
    case '*':
    case '/':
      return 3;
  }

  return -1;
}

std::string convert_to_postfix_form(std::string str_inf) {
  TStack operations(MAX_STACK_AM);
  std::string str_postfix;
  int curr_priority, prev_priority;
  char curr_symb, temp_symb;
  bool work_with_operand;

  for (std::string::iterator iter = str_inf.begin(); iter != str_inf.end();
  ++iter) {
    if (*iter == ' ')
      str_inf.erase(iter);
  }

  for (std::string::iterator iter = str_inf.begin(); iter != str_inf.end();
  ++iter) {
    curr_priority = get_priority(*iter);
    curr_symb = *iter;

    if (curr_priority == -1) {
      work_with_operand = true;
      str_postfix += curr_symb;
    } else {
      if (work_with_operand) {
        work_with_operand = false;
        str_postfix += ' ';
      }

      if (curr_symb == ')') {
        while (get_priority(temp_symb = operations.Get()) != 0) {
          str_postfix += temp_symb;
        }

        operations.Get(); // выталкивание открывающей скобки
      } else if (curr_priority == 0 || curr_priority > prev_priority
      || operations.IsEmpty()) {
        operations.Put(curr_symb);
      } else {
        while (get_priority(temp_symb = operations.Get()) >= curr_priority) {
          str_postfix += temp_symb;
        }

        str_postfix += ' ';
        operations.Put(curr_symb);
      }
    }

    prev_priority = curr_priority;
  }  // for

  while (!operations.IsEmpty()) {
    str_postfix += operations.Get();
  }

  return str_postfix;
}

double compute(std::string str) {
  if (!is_correct(str))
    return 0;

  str = convert_to_postfix_form(str);

  TSimpleStack<double, MAX_STACK_AM> operands;
  std::string temp;
  int curr_priority;
  double first_operand, sec_operand, res;

  for (std::string::iterator iter = str.begin(); iter != str.end(); ++iter) {
    curr_priority = get_priority(*iter);

    // Если данный символ - часть операнда
    if (curr_priority == -1 && *iter != ' ') {
      temp += *iter;
    } else {
      if (temp.length()) {
        operands.push(atof(temp.c_str()));
        temp = "";
      }

      if (curr_priority >= 2) {
        sec_operand = operands.pop();
        first_operand = operands.pop();

        switch (*iter) {
        case '+':
          res = first_operand + sec_operand;
          break;
        case '-':
          res = first_operand - sec_operand;
          break;
        case '*':
          res = first_operand * sec_operand;
          break;
        case '/':
          res = first_operand / sec_operand;
          break;
        }

        operands.push(res);
      }
    }  // if-else
  }  // for

  return res;
}
