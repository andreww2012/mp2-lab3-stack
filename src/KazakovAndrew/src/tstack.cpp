#include <iostream>
#include "tstack.h"

TStack::TStack(const int size): TDataRoot(size), top_(-1) {}

void TStack::Put(const int& value) {
  if (memory_ == nullptr) {
    throw 1;
  }

  if (IsFull()) {
    throw 2;
  }

  memory_[++top_] = value;
  amount_++;
}

int TStack::Get() {
  if (memory_ == nullptr) {
    throw 1;
  }

  if (IsEmpty()) {
    throw 3;
  }

  amount_--;

  return memory_[top_--];
}

void TStack::Print() const {
  for (int i = 0; i < amount_; i++) {
    std::cout << memory_[i] << " ";
  }

  std::cout << std::endl;
}
